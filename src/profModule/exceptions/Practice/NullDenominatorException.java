package profModule.exceptions.Practice;

public class NullDenominatorException extends Exception{
    public NullDenominatorException(String message) {
        super(message);
    }
}
