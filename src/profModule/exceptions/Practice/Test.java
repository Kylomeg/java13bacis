package profModule.exceptions.Practice;

public class Test {
    public static void main(String[] args) {
        try {
            Fraction fraction = new Fraction(1, 3);
            System.out.println(fraction);
        }
        catch (NullDenominatorException e) {
            System.out.println(e.getMessage());
        }
        }
}
