package profModule.exceptions.Tasks;
/*
С помощью двух массивов, как показано ниже, напишите программу, которая предложит пользователю ввести целое число
от 1 до 12, а затем отобразит месяц и количество дней, соответствующие этому целому числу. Если пользователь вводит
недопустимое число, то программа должна отображать Недопустимое число с помощью перехвата ArrayIndexOutOfBoundsException.
 */

import java.util.Scanner;

public class Task1 {
    private static final String[] months = {"январь", "февраль", "март", "апрель",
            "май", "июнь", "июль", "август", "сентябрь", "октябрь", "ноябрь", "декабрь"};

    private static final int[] dom = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    /**
     * Метод main
     */
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        while (true) {
            System.out.print("Введите порядковый номер месяца, от 1 до 12: ");
            // Получить порядковый номер месяца
            int monthNumber = input.nextInt();

            try {
                // Отобразить название и количество дней в соответствующем месяце
                System.out.println("Месяц: " + months[monthNumber - 1]);
                System.out.println("Количество дней: " + dom[monthNumber - 1]);
            } catch (ArrayIndexOutOfBoundsException e) {
                // Отобразить сообщение, если введен неверный номер месяца
                System.out.println("Недопустимое число");
                // Считать символ новой строчки, созданный нажатием «Enter» после числа
                input.nextLine();
            }
        }
    }
}

