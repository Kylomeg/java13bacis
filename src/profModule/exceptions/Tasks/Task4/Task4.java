package profModule.exceptions.Tasks.Task4;

public class Task4 {
    public static void main(String[] args) {
        try {
            TriangleWithException t1 = new TriangleWithException(1.5, 2, 3);
            System.out.println("Периметр t1: " + t1.getPerimeter());
            System.out.println("Площадь t1: " + t1.getArea());

            TriangleWithException t2 = new TriangleWithException(1, 2, 3);
            System.out.println("Периметр t2: " + t2.getPerimeter());
            System.out.println("Площадь t2: " + t2.getArea());
        }
        catch (IllegalTriangleException ex) {
            System.out.println("Недопустимый треугольник");
            System.out.println("Сторона 1: " + ex.getSide1());
            System.out.println("Сторона 2: " + ex.getSide2());
            System.out.println("Сторона 3: " + ex.getSide3());
        }
    }
}

