package profModule.exceptions.Tasks.Task4;


// Назовите новый класс Triangle как TrianlgeWithException, чтобы избежать
// конфликта имен с уже определенным классом Triangle
class TriangleWithException extends Object {
    double side1, side2, side3;

    /** Конструктор */
    public TriangleWithException(double side1, double side2, double side3)
            throws IllegalTriangleException {
        this.side1 = side1;
        this.side2 = side2;
        this.side3 = side3;
        if (side1 + side2 <= side3 || side1 + side3 <= side2 || side2 + side3 <= side1)
            throw new IllegalTriangleException(side1, side2, side3,
                    "Сумма двух сторон больше третьей");
    }

    /** Реализует абстрактный метод findArea класса GeometricObject */
    public double getArea() {
        double s = (side1 + side2 + side3) / 2;
        return Math.sqrt(s * (s - side1) * (s - side2) * (s - side3));
    }

    /** Реализует абстрактный метод findCircumference класса GeometricObject **/
    public double getPerimeter() {
        return side1 + side2 + side3;
    }

    @Override
    public String toString() {
        return "Треугольник: сторона 1 = " + side1 + " сторона 2 = " + side2 +
                " сторона 3 = " + side3;
    }
}