package profModule.homework4;

import java.util.List;

public class Task2 {
    public static void main(String[] args) {
        List<Integer> list = List.of(8, 2 , 1, 9, 11);
        System.out.println(list.stream().reduce((a,b) -> a * b).get());
    }
}
