package profModule.homework4;

import java.util.stream.IntStream;

public class Task1 {
    public static void main(String[] args) {
        System.out.println(IntStream.rangeClosed(1,100).filter(n -> n % 2 == 0).sum());
    }
}
