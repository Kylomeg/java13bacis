package profModule.homework4;

import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

public class Task4 {
    public static void main(String[] args) {
        List<Integer> list = List.of(8, 12, 0, 1, 3, -1, -5);
        Stream<Integer> sorted = list.stream().sorted(Collections.reverseOrder());
        sorted.forEach(System.out::println);
    }
}
