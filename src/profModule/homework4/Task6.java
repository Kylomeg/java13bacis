package profModule.homework4;

import java.util.Set;
import java.util.stream.Collectors;

public class Task6 {
    public static void main(String[] args) {
        Set<Integer> set1 = Set.of(1, 2, 3);
        Set<Integer> set2 = Set.of(4, 5, 6);
        Set<Integer> set3 = Set.of(7, 8, 9);
        Set<Set<Integer>> set = Set.of(set1, set2, set3);
        Set<Integer> result = set.stream().flatMap(Set::stream).collect(Collectors.toSet());
        System.out.println(result);
    }
}
