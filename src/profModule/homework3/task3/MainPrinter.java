package profModule.homework3.task3;

import java.lang.reflect.*;

public class MainPrinter {
    public static void main(String[] args) {
        try {
            Class<?> cls = APrinter.class;
            Method method = cls.getMethod("print", int.class);
            method.invoke(new APrinter(), 101);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            System.out.println("Ошибка: " + e.getMessage());
        }
    }

}
