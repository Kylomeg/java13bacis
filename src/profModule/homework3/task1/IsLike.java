package profModule.homework3.task1;

import java.lang.annotation.*;

    @Target(ElementType.TYPE)
    @Retention(RetentionPolicy.RUNTIME)
    public @interface IsLike {
        boolean like();
    }
