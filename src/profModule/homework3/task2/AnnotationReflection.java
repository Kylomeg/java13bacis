package profModule.homework3.task2;

import profModule.homework3.task1.IsLike;
import java.lang.annotation.Annotation;

public class AnnotationReflection {
    public static void main(String[] args) {
            printClassValue(LikeClass.class);
        }

        public static void printClassValue(Class<?> cls) {
            if(!cls.isAnnotationPresent(IsLike.class)) {
                return;
            }
            IsLike isLike = cls.getAnnotation(IsLike.class);
            System.out.println("Значение=" + isLike.like());
        }
    }

