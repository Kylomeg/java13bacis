package profModule.homework3.task4;
import java.util.*;

public class Pedigree {
    public static void main(String[] args) {
        List<Class<?>> result = getAllInterfaces(C.class);
        result.forEach(System.out::println);

    }

    public static List<Class<?>> getAllInterfaces(Class<?> cls) {
        List<Class<?>> interfaces = new ArrayList<>();
        while (cls != Object.class) {
            for (Class<?> inter: cls.getInterfaces()) {
                interfaces.add(inter);
                List<Class<?>> listInter = List.of(inter.getInterfaces());
                while (listInter.size() > 0) {
                    for (Class<?> in: listInter) {
                        inter = in;
                        interfaces.add(inter);
                        listInter = List.of(inter.getInterfaces());
                    }
                }
            }
            cls = cls.getSuperclass();
        }
        return interfaces;
    }

}
