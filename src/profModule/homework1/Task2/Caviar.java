package profModule.homework1.Task2;

public class Caviar {
    private String name;
    private int weight;
    private String from;
    public Caviar(){
        this.name="";
        this.weight=0;
        this.from="";
    }
    public Caviar(String name, int weight, String from) {
        this.name = name;
        this.weight = weight;
        this.from = from;
    }
    @Override
    public String toString(){
        return this.name +" "+ this.from + " " + this.weight;
}
}
