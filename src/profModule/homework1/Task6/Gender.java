package profModule.homework1.Task6;

public enum Gender {

    FEMALE("Мужской"),
    MALE("Женский");
    private String str;
    Gender(String str) {
        this.str = str;
    }
    public String getStr() {
        return str;
    }
    public void setStr(String str) {
        this.str = str;
    }
}
