package profModule.homework1.Task6;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;
import static profModule.homework1.Task6.Gender.*;



public class Validator {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        try {
            System.out.println("ведите имя:");
            String name = scanner.nextLine();
            checkName(name);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }


        try {
            System.out.println("ведите дату рождения:");
            String birthDate = scanner.nextLine();
            checkBirthdate(birthDate);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        try {
            System.out.println("ведите пол:");
            String gender = scanner.nextLine();
            checkGender(gender);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        try {
            System.out.println("ведите рост:");
            String height = scanner.nextLine();
            checkHeight(height);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }


    }
    public static void checkName (String str) throws Exception {
        if(str.matches("[А-Я]{1}[а-яё]{1,17}|[A-Z]{1}[a-z]{1,17}")) {
            System.out.println(str);
        }
        else {
            throw new Exception("Имя не соответствует формату!");
        }
    }
    public static void checkBirthdate(String date) throws Exception {
        if (date.matches("\\d\\d\\.\\d\\d\\.\\d\\d\\d\\d")) {

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.MM.yyyy");
            LocalDate localDate = LocalDate.parse(date, formatter);

            if (localDate.isBefore(LocalDate.now()) && localDate.isAfter(LocalDate.parse("01.01.1900", formatter))) {
                System.out.println(date);
            }
            else {
                throw new RuntimeException("Дата рождения должна быть не раньше 01.01.1900");
            }

        } else {
            throw new Exception("Ошибка! Неправильно ввели дату. (dd.mm.yyyy)");
        }
    }

    public static void checkGender(String str) throws Exception {
        if(str.matches(MALE.getStr()) || str.matches(FEMALE.getStr())) {
            System.out.println("Пол: " + str);
        } else {
            throw new Exception("Только Мужской или Женский пол");
        }
    }

    public static void checkHeight(String str) {
        Double height = Double.parseDouble(str);
        if (height > 0) {
            System.out.println(height);
        }
        else {
            throw new ArithmeticException("Рост должен быть положительным числом!");
        }
    }
}
