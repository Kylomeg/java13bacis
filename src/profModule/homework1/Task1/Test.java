package profModule.homework1.Task1;

public class Test {
    public static void main(String[] args) {
        Celebration newYear = new Celebration();
        newYear.buyOneChampagneBottle();
        newYear.decorateTree();
        try {
            System.out.println(newYear.startUnstoppableFun());
        } catch (MyCheckedException e) {
            System.out.println("Не можем начать праздновать: " + e.getMessage());
        }
        System.out.println("Накрываем стол");
        newYear.setTheTable();
        try {
            System.out.println(newYear.startUnstoppableFun());
        } catch (MyCheckedException e) {
            System.out.println("Не можем начать праздновать: " + e.getMessage());
        }

    }
}
