package profModule.homework1.Task1;

public class MyCheckedException extends Exception{
    public MyCheckedException(String message){
        super(message);
    }
}
