package profModule.homework1.Task1;

public class Celebration {
    private int champagneBottles;
    private boolean treeIsDecorated;
    private boolean tableIsSet;
    public Celebration(){
        this.champagneBottles = 0;
        this.tableIsSet=false;
        this.treeIsDecorated=false;
    }

    public void buyOneChampagneBottle() {
        this.champagneBottles++;
    }
    public void decorateTree() {
        this.treeIsDecorated = true;
    }
    public void setTheTable(){
        this.tableIsSet = true;
    }
    public String startUnstoppableFun() throws MyCheckedException{
        if (tableIsSet && treeIsDecorated && champagneBottles > 0) {
            return "Да начнётся безудержное веселье!!!";
        }else{
            throw new MyCheckedException("Стол накрыт: " + tableIsSet +
                    ". Бутылок шампанского: "+ champagneBottles + ". Елка наряжена:" + treeIsDecorated +".");
        }
    }
}
