package profModule.homework1.Task3;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class CapitalLettersFileWriter {
    private static String folder = "C:\\CapitalLetterFolder";
    private static String outputFileName = "output.txt";
    private static ArrayList<String> lines = new ArrayList<>();

    public static void fileReader(String filePath) {
        try(Scanner scanner = new Scanner(new File(filePath))) {
            while (scanner.hasNextLine()) {
                lines.add(scanner.nextLine());
            }
        } catch (FileNotFoundException e) {
            System.out.println("Файл не найден!");
        }
    }
    public static void fileWriter() throws IOException {
        try (Writer writer = new FileWriter(folder + "\\" + outputFileName)) {
            for (String line: lines) {
                writer.write(line.toUpperCase() + "\n");
            }
        } catch (IOException e) {
            System.out.println("Ошибка записи файла");
        }
    }
}
