package profModule.homework1.Task4;

public class MyEvenNumber {
    int num;
    public MyEvenNumber(int num) {
        createEvenNumber(num);
    }

    private void createEvenNumber(int num) {
        if ( num % 2 == 0) {
            this.num = num;
        }
        else {
            throw new RuntimeException(String.valueOf(num));
        }

        }
    }

