package profModule.homework1.Task4;

public class Test {
    public static void main(String[] args){
        try {
            MyEvenNumber n1 = new MyEvenNumber(2);
            System.out.println(n1);
            MyEvenNumber n2 = new MyEvenNumber(1);
            System.out.println(n2);
        } catch (RuntimeException e){
            System.out.println("Невозможно создать объект с нечётным числом");
        }
    }
}
