package profModule.homework2.Task3;
import java.util.*;
public class PowerfulSet {
    public static void main(String[] args) {
        Set<Integer> s1 = Set.of(1, 2, 3);
        Set<Integer> s2 = Set.of(0, 1, 2, 4);
        PowerfulSet powerfulSet = new PowerfulSet();
        System.out.println(powerfulSet.intersection(s1, s2));
        System.out.println(powerfulSet.union(s1, s2));
        System.out.println(powerfulSet.relativeComplement(s1, s2));
    }
    public <T> Set<T> intersection(Set<T> set1, Set<T> set2) {
        Set<T> set = new HashSet<>(set1);
        set.retainAll(set2);
        return set;
    }

    public <T> Set<T> union(Set<T> set1, Set<T> set2) {
        Set<T> set = new HashSet<>(set1);
        set.addAll(set2);
        return set;
    }
    public <T> Set<T>  relativeComplement(Set<T> set1, Set<T> set2) {
        Set<T> set = new HashSet<>(set1);
        set.removeAll(set2);
        return set;
    }
}


