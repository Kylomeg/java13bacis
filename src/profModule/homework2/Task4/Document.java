package profModule.homework2.Task4;

import java.util.*;

public class Document {
    public static void main(String[] args) {
        List<Document> docs = new ArrayList<>();
        docs.add(new Document(1, "Съешь ещё этих мягких французских булок!", 1));
        docs.add(new Document(2, "Съешь ещё этих жёстских французских булок!", 2));
        docs.forEach(System.out::println);
        System.out.println(organizeDocuments(docs));


    }

    public int id;
    public String name;
    public int pageCount;

    @Override
    public String toString() {
        return "id:" + id + ". text: " + name;
    }

    public Document(int id, String name, int pageCount) {
        this.id = id;
        this.name = name;
        this.pageCount = pageCount;
    }

    public static Map<Integer, Document> organizeDocuments(List<Document> documents) {
        Map<Integer, Document> docMap = new HashMap<>();
        for (Document doc : documents) {
            docMap.put(doc.id, doc);
        }
        return docMap;
    }
}


