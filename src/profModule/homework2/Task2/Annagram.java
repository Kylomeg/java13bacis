package profModule.homework2.Task2;
import java.util.*;
public class Annagram {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите S строку:");
        String s = scanner.next();
        System.out.println("Введите N строку:");
        String n = scanner.next();
        System.out.println("Строка S является аннаграммой строки N: " + isAnnagram(s, n));
    }

    private static boolean isAnnagram(String s, String n) {
        ArrayList<Character> charMass1 = new ArrayList<>();
        char[] mass1 = s.toCharArray();
        for (char m: mass1) {
            charMass1.add(m);
        }
        Collections.sort(charMass1);
        ArrayList<Character> charMass2 = new ArrayList<>();
        char[] mass2 = n.toCharArray();
        for (char m: mass2) {
            charMass2.add(m);
        }
        Collections.sort(charMass2);

        return charMass1.equals(charMass2);
    }
}
