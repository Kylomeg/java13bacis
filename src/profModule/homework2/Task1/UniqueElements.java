package profModule.homework2.Task1;

import java.util.*;


public class UniqueElements {
    public static <T> Set<T> getSet(List<T> list) {
        return new HashSet<>(list);
    }
}