package Project;

import java.util.Date;

public class Account {
    private int id;
    private double balance;
    private static double annualInterestRate;
    private Date dateCreated;

    /**
     * Создаёт по умолчанию заданный банковский счёт
     */
    public Account(int id, double balance) {
        this.id = id;
        this.balance = balance;
        dateCreated = new Date();
    }

    /**
     * Возвращяет id
     */
    public double getId() {
        return id;
    }

    /**
     * Возврящает баланс
     */
    public double getBalance() {
        return balance;
    }

    /**
     * Возвращает годовую процентную ставку
     */
    public static double getAnnualInterestRate() {
        return getAnnualInterestRate();
    }

    /**
     * Возвращает дату создания счёта
     */
    public Date getDateCreated() {
        return dateCreated;
    }

    /**
     * Присваивает новый id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Присваивает новый баланс
     */
    public void setBalance(double balance) {
        this.balance = balance;
    }

    /** Присваивает новую годовую процентную ставку */
    public static void setAnnualInterestRate(double annualInterestRate) {
        Account.annualInterestRate = annualInterestRate;
    }

    /**
     * Возвращает ежемесячный процент
     */
    public double getMountlyInterest() {
        return balance * (annualInterestRate / 1200);
    }

    /**
     * Снимает деньги с депозита
     */
    public void withdraw(double amount) {
        balance -= amount;
    }

    /**
     * Пополняет депозит
     */
    public void deposit(double amount) {
        balance += amount;
    }

}
