package Project;

import java.util.Scanner;
public class ConvertCny3 {
    static final double CNY_PER_RUB = 79.64;

    public static void main(String[] args) {
//        final double CNY_PER_RUB = 79.64;
        Scanner input = new Scanner(System.in);
        int[] cnyArray;
        int i, n;
        double[] rubArray;
        instruct();

        do {
            System.out.println("Введите количество конвертаций: ");
            n = input.nextInt();
        } while (n <= 0);
//        Получаем количество конвертаций пока не введено правильное значение

        System.out.println("Получить " + n + " сумм денег в китайских юанях через пробел: ");
        cnyArray = new int[n];
        for (i = 0; i < n; ++i) {
            cnyArray[i] = input.nextInt();
        }
//         Конвертировать
        rubArray = findRub(cnyArray, n);

    
//    Отобразить таблицу конвертаций
    
        System.out.println("Сумма,  Руб.\tСумма,  Cny");
        for (i = 0; i < n; ++i)
        System.out.println("\t\t" + (int)(rubArray[i] * 100) / 100.0 + "\t\t\t" + cnyArray[i]);
}


    public static void instruct() {
        System.out.println("Эта программа конвертирует сумму денег из " + "китайских юаней в российские рубли");
        System.out.println("Курс покупки равен " + CNY_PER_RUB + " рубля.\n");
    }
    public  static double[] findRub(int[] cnyArray, int n) {
       double[] rubArray = new double[n];
        int i;
        for (i = 0; i < n; ++i)
            rubArray[i] = CNY_PER_RUB * cnyArray[i];
        return rubArray;
    }
}
