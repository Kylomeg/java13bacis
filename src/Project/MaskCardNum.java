package Project;

import java.util.Scanner;

public class MaskCardNum {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String cardNum;
        System.out.println("Введите номер карты: ");
        cardNum = input.next();
        String updCardNum = maskChar(cardNum, 4);

        //Выводим маскированный номер
        System.out.println("Маскированный номер: " + updCardNum);
    }

    /**
     * Замена символов строоки символом '*'
     *
     * @param input
     * @param numFromEnd
     * @return
     */
    static String maskChar(String input, int numFromEnd) {
        //Если количество отображаемых символов меньше 0 или равно длинне строки
        if (numFromEnd < 0 || numFromEnd >= input.length()) {
            return input;
        }
        //Заменяем первый эдемент строки и делаем рекурсивный вызов
        return "*" + maskChar(input.substring(1), numFromEnd);
    }
}
