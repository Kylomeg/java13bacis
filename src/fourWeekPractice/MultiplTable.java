package fourWeekPractice;

public class MultiplTable {
    public static void main(String[] args) {
        int a, b;
        for (a = 0; a <= 9; ++a) {
            System.out.println();
            for (b = 0; b <= 9; ++b) {
                System.out.print(" " + a * b);
            }
        }
    }
}
