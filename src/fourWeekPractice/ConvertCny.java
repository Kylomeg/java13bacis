package fourWeekPractice;

import java.util.Scanner;

public class ConvertCny {
    public static void main(String[] args) {
        final double CNY_PER_RUB = 79.64;
        Scanner input = new Scanner(System.in);
        int cny, digit, i, n;
        double rub;

//        Получаем количество конвертаций пока не введено правильное значение
        do {
            System.out.println("Введите количество конвертаций: ");
            n = input.nextInt();
        } while (n <= 0);

        for (i = 0; i < n; ++i) {

            System.out.print("Введите сумму денег в китайских юанях: ");
            cny = input.nextInt();

            System.out.print(cny);

            if (5 <= cny && cny <= 20)
                System.out.print(" китайских юаней равны ");
            else {
                digit = cny % 10;

                if (digit == 1)
                    System.out.print(" китайский юань равен ");
                else if (2 <= digit && digit <= 4) {
                    System.out.print(" китайских юаня равны ");
                } else
                    System.out.print(" китайских юаней равны ");
            }
            rub = CNY_PER_RUB * cny;
            System.out.println(rub + " российских рублей.");
        }
    }
}