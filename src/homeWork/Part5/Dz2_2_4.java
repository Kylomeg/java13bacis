package homeWork.Part5;
/*
На вход подается число N — количество строк и столбцов матрицы. Затем
передается сама матрица, состоящая из натуральных чисел. После этого
передается натуральное число P.
Необходимо найти элемент P в матрице и удалить столбец и строку его
содержащий (т.е. сохранить и вывести на экран массив меньшей размерности).
Гарантируется, что искомый элемент единственный в массиве.
*/

import java.util.Arrays;
import java.util.Scanner;

public class Dz2_2_4 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int mass = input.nextInt();
        int[][] m = new int[mass][mass];
        int xK = 0;
        int yK = 0;
        for (int a = 0; a < mass; a++) {
            for (int b = 0; b < mass; b++) {
                m[a][b] = input.nextInt();
            }
        }
        int num = input.nextInt();

        //Найдём в где в массиве искомое число
        for (int a = 0; a < mass; a++) {
            for (int b = 0; b < mass; b++) {
                if (m[a][b] == num) {
                    xK = b;
                    yK = a;
                }
            }
        }


        for (int a = 0; a < mass; a++) { //Вывод массива без искомой строки и столбца
            if (a != yK) {
                for (int b = 0; b < mass; b++) {
                    if (b != xK) {
                        if (b != 0)
                            System.out.print(" ");
                        System.out.print(m[a][b]);

                    }
                        if (b == mass - 1)
                            System.out.print("\n");

                }
            }
        }
    }
}