package homeWork.Part5;

import java.util.Scanner;
/*
На вход подается число N. Необходимо вывести цифры числа слева направо.
Решить задачу нужно через рекурсию.
*/
public class Dz2_2_9 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int a = input.nextInt();
        sum(a);
    }
    public static void sum(int a) {
        if (a / 10 == 0){ //Если "a" имеет 1 разряд - вывести его в консоль
            System.out.print(a);
            return;
        }
        int b = a % 10;// Отрезаем последний разряд и выводим пробел + последний разряд
        a /= 10;
        sum(a);
        System.out.print(" " + b);
    }
}