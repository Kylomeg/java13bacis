package homeWork.Part5;
/*
На вход передается N — количество столбцов в двумерном массиве и M —
количество строк. Затем сам передается двумерный массив, состоящий из
натуральных чисел.
Необходимо сохранить в одномерном массиве и вывести на экран
минимальный элемент каждой строки.
*/

import java.util.Scanner;

public class Dz2_2_1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int y = input.nextInt();
        int x = input.nextInt();
        int[][] m = new int[x][y];
        int[] min = new int[x];
        for (int a = 0; a < x; a++) {
            for (int b = 0; b < y; b++) {
                m[a][b] = input.nextInt();
            }
        }
        for (int a = 0; a < x; a++)
            min[a] = 1000;
        for (int a = 0; a < x; a++) {
            for (int b = 0; b < y; b++) { // Находим минимум в каждой строке
                if (min[a] > m[a][b])
                    min[a] = m[a][b];
            }
        }
        for (int a = 0; a < x; a++)
            System.out.print(min[a] + " ");
    }
}
