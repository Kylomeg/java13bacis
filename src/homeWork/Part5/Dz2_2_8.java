package homeWork.Part5;

import java.util.Scanner;

/*
На вход подается число N. Необходимо посчитать и вывести на экран сумму его
цифр. Решить задачу нужно через рекурсию.
*/

public class Dz2_2_8 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int a = input.nextInt();
        System.out.println(sum(a));
    }

    public static int sum(int a) {
        if (a / 10 == 0){ //Если "a" имеет 1 разряд - вернуть "а"
            return a;
        }
        int b = a % 10;// Отрезаем последний разряд и суммируем рекурсией с оставшимся числом
        a /= 10;
        b += sum(a);
        return b;

    }
}