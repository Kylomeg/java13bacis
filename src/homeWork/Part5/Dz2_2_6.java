package homeWork.Part5;
/*
Петя решил начать следить за своей фигурой. Но все существующие
приложения для подсчета калорий ему не понравились и он решил написать
свое. Петя хочет каждый день записывать сколько белков, жиров, углеводов и
калорий он съел, а в конце недели приложение должно его уведомлять,
вписался ли он в свою норму или нет.
На вход подаются числа A — недельная норма белков, B — недельная норма
жиров, C — недельная норма углеводов и K — недельная норма калорий.
Затем передаются 7 строк, в которых в том же порядке указаны сколько было
съедено Петей нутриентов в каждый день недели. Если за неделю в сумме по
каждому нутриенту не превышена недельная норма, то вывести “Отлично”,
иначе вывести “Нужно есть поменьше”.
*/

import java.util.Scanner;

public class Dz2_2_6 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int[][] m = new int[8][4];
        int a = 0, b = 0, c = 0, d = 0;
        for (int i = 0; i < 8; i++)
            for (int j = 0; j < 4; j++) {
                m[i][j] = input.nextInt();
            }
        for (int i = 1; i < 8; i++) {
            a += m[i][0];
            b += m[i][1];
            c += m[i][2];
            d += m[i][3];
        }
        if (a <= m[0][0] && b <= m[0][1] && c <= m[0][2] && d <= m[0][3]) {
            System.out.println("Отлично");
        } else {
            System.out.println("Нужно есть поменьше");
        }
    }
}