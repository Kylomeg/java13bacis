package homeWork.Part5;
/*
На вход подается число N — количество строк и столбцов матрицы.
Затем передается сама матрица, состоящая из натуральных чисел.
Необходимо вывести true, если она является симметричной относительно
побочной диагонали, false иначе.
Побочной диагональю называется диагональ, проходящая из верхнего правого
угла в левый нижний.
*/
import java.util.Arrays;
import java.util.Scanner;

public class Dz2_2_5 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int mass = input.nextInt();
        int[][] m = new int[mass][mass];
        boolean flag = true;
        for (int a = 0; a < mass; a++) {
            for (int b = 0; b < mass; b++) {
                m[a][b] = input.nextInt();
            }
        }
        int a, j, i, b; // Сравниваем массив слева-направо и снизу-вверх
        for (a = 0, i = mass - 1; a < mass - 1; a++, i--) {
            for (b = 0, j = mass - 1; b < mass - 1; b++, j--) {
                if (m[a][b] != m[j][i]) { //Возвращяем false после первого несовпадения
                    flag = false;
                    System.out.println(flag);
                    return;
                }
            }
        }
        System.out.println(flag);
    }
}