package homeWork.Part5;

import java.util.Scanner;

/*
На вход подается число N — количество строк и столбцов матрицы. Затем в
последующих двух строках подаются координаты X (номер столбца) и Y (номер
строки) точек, которые задают прямоугольник.
Необходимо отобразить прямоугольник с помощью символа 1 в матрице,
заполненной нулями (см. пример) и вывести всю матрицу на экран.
*/
public class Dz2_2_2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int mass = input.nextInt();
        int x1 = input.nextInt();
        int y1 = input.nextInt();
        int x2 = input.nextInt();
        int y2 = input.nextInt();
        int[][] m = new int[mass][mass];
        for (int a = 0; a < mass; a++) {
            for (int b = 0; b < mass; b++) {
                m[a][b] = 0; // Заполняем массив нулями
            }
        }
        for (int b = x1; b <= x2; b++) // Рисуем 4 линии единицами
            m[y1][b] = 1;
        for (int b = x1; b <= x2; b++)
            m[y2][b] = 1;
        for (int b = y1; b <= y2; b++)
            m[b][x1] = 1;
        for (int b = y1; b <= y2; b++)
            m[b][x2] = 1;

        for (int a = 0; a < mass; a++) {
            for (int b = 0; b < mass; b++) {
                System.out.print(m[a][b]);
                if (b < mass - 1)
                    System.out.print(" "); //Добавляем пробел только мешду символами и перенос строки
                if (b == mass - 1)
                    System.out.print("\n");
            }
        }
    }
}
