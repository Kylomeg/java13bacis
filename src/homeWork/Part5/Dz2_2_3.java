package homeWork.Part5;

import java.util.Scanner;

/*
На вход подается число N — количество строк и столбцов матрицы. Затем
передаются координаты X и Y расположения коня на шахматной доске.
Необходимо заполнить матрицу размера NxN нулями, местоположение коня
отметить символом K, а позиции, которые он может бить, символом X.
*/
public class Dz2_2_3 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int mass = input.nextInt();
        int x1 = input.nextInt();
        int y1 = input.nextInt();
        int xK, yK;
        char[][] m = new char[mass][mass];
        for (int a = 0; a < mass; a++) {
            for (int b = 0; b < mass; b++) {
                m[a][b] = '0'; // Заполняем массив нулями
            }
        }
        m[y1][x1] = 'K';

        if (x1 - 2 >= 0) { //Влево
            xK = x1 - 2;
            if (y1 - 1 >= 0)
                m[y1 - 1][xK] = 'X';
            if (y1 + 1 < m.length)
                m[y1 + 1][xK] = 'X';
        }
        if (x1 + 2 < m[1].length) { //Вправо
            xK = x1 + 2;
            if (y1 - 1 >= 0)
                m[y1 - 1][xK] = 'X';
            if (y1 + 1 < m.length)
                m[y1 + 1][xK] = 'X';
        }
        if (y1 - 2 >= 0) { //Вверх
            yK = y1 - 2;
            if (x1 - 1 >= 0)
                m[yK][x1 - 1] = 'X';
            if (x1 + 1 < m[1].length)
                m[yK][x1 + 1] = 'X';
        }
        if (y1 + 2 < m.length) { //Вниз
            yK = y1 + 2;
            if (x1 - 1 >= 0)
                m[yK][x1 - 1] = 'X';
            if (x1 + 1 < m[1].length)
                m[yK][x1 + 1] = 'X';
        }

        for (int a = 0; a < mass; a++) {
            for (int b = 0; b < mass; b++) {
                System.out.print(m[a][b]);
                if (b < mass - 1)
                    System.out.print(" "); //Добавляем пробел только мешду символами и перенос строки
                if (b == mass - 1)
                    System.out.print("\n");

            }
        }
    }
}