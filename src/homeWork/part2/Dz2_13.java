package homeWork.part2;

import java.util.Scanner;

public class Dz2_13 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String inp1 = input.nextLine();
        boolean stone = inp1.contains("камни!");
        boolean zapr = inp1.contains("запрещенная\sпродукция");
        if (stone && zapr) {
            System.out.println("в посылке камни и запрещенная продукция");
        } else if (stone) {
            System.out.println("камни в посылке");
        } else if (zapr) {
            System.out.println("в посылке запрещенная продукция");
        } else {
            System.out.println("все ок");
        }
    }
}
