package homeWork.part2;

import java.util.Scanner;

public class Dz2_8 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String s = input.nextLine();
        int k = s.lastIndexOf(' ');  //Выясняем какой по счёту символ последний пробел
        System.out.println(s.substring(0, k));
        System.out.println(s.substring(k + 1));
    }
}
