package homeWork.part2;

import java.util.Scanner;

public class Dz2_14 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String inp1 = input.nextLine();
        int price = input.nextInt();
        boolean model = inp1.contains("samsung") || inp1.contains("iphone");
//        int priceNum = inp1.lastIndexOf("\s");
//        String price = inp1.substring(priceNum);
        if (price >= 50000 && price <= 120000 && model) {
            System.out.println("Можно купить");
        } else {
            System.out.println("Не подходит");
        }
    }
}
