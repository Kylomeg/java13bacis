package homeWork.part2;

import java.util.Scanner;

public class Dz2_9 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int x = input.nextInt();
        double a, b, z;
        a = Math.pow(Math.sin(x), 2);
        b = Math.pow(Math.cos(x), 2);
        z = a + b - 1;
        final double EPS = 1E-15;
        boolean c = (z == 0);
        if (c) {
            System.out.println(c);
        } else {
            c = Math.abs(z) < EPS;
            System.out.println(c);

        }
    }
}