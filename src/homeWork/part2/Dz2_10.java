package homeWork.part2;

import java.util.Scanner;

public class Dz2_10 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        final double EXP = 1E-10;
        double a = input.nextDouble();
        double b = Math.log(Math.pow(Math.E, a));
//        double b = Math.pow(Math.E, log);
        boolean c = (b == a);
        if (c) {
            System.out.println(c);
        } else {
            c = Math.abs(a - b) < EXP;
            System.out.println(c);
        }
    }
}