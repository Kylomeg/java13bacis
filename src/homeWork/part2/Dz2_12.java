package homeWork.part2;

import java.util.Scanner;

public class Dz2_12 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String pass = input.nextLine();
        boolean lowCase = pass.length() > 0 && pass.matches(".*[a-z].*");
        boolean highCase = pass.matches(".*[A-Z].*");
        boolean specCase = pass.matches(".*[-_*].*");
        boolean numCase = pass.matches(".*[\\d].*");

        if (lowCase && highCase && specCase && numCase)
//        if ("Hello_22".matches(".*[_*-].*"))
        {
            System.out.println("пароль надежный");
        } else {
            System.out.println("пароль не прошел проверку");
        }
    }
}
