package homeWork.part3;

import java.util.Scanner;

/*
В банкомате остались только купюры номиналом 8 4 2 1. Дано положительное
число n - количество денег для размена. Необходимо найти минимальное
количество купюр с помощью которых можно разменять это количество денег
(соблюсти порядок: первым числом вывести количество купюр номиналом 8,
вторым - 4 и т д)
*/
public class Dz3_6 {
    public static void main(String[] args) {
        int m, k8, k4, k2, k1;
        k4 = k2 = k8 = k1 = 0;
//        int summ = 0;
        Scanner input = new Scanner(System.in);
        m = input.nextInt();
        for (k8 = 0; m >= 8; k8++)
            m -= 8;
        for (k4 = 0; m >= 4; k4++)
            m -= 4;
        for (k2 = 0; m >= 2; k2++)
            m -= 2;
        for (k1 = 0; m >= 1; k1++)
            m -= 1;

        System.out.print(k8 + " " + k4 + " " + k2 + " " + k1);

    }
}

