package homeWork.part3;

import java.util.Scanner;

/*
Даны положительные натуральные числа m и n. Найти остаток от деления m на
n, не выполняя операцию взятия остатка.
*/

public class Dz3_5 {
    public static void main(String[] args) {
        int m, n;
        int summ = 0;
        Scanner input = new Scanner(System.in);
        m = input.nextInt();
        n = input.nextInt();
        summ = m;
        for (int a = 0; summ >= n; a++)
            summ -= n;

        System.out.println(summ);

    }
}

