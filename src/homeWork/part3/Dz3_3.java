package homeWork.part3;

import java.util.Scanner;

/*
На вход подается два положительных числа m и n. Необходимо вычислить m^1
+ m^2 + ... + m^n
*/
public class Dz3_3 {
    public static void main(String[] args) {
        int m, n;
        int summ = 0;
        Scanner input = new Scanner(System.in);
        m = input.nextInt();
        n = input.nextInt();
        for (int a = 1; a <= n; a++)
            summ += Math.pow(m, a);

        System.out.println(summ);

    }
}