package homeWork.part3;

import java.util.Scanner;

/* Дано натуральное число n. Вывести его цифры в “столбик”. */

public class Dz3_4 {
    public static void main(String[] args) {
        String m;

        Scanner input = new Scanner(System.in);
        m = input.nextLine();
        for (int a = 0; a <= m.length() - 1; a++)
            System.out.println(m.charAt(a));

    }
}

