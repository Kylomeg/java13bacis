package homeWork.part3;

/*
Вывести на экран “ёлочку” из символа звездочки (*) заданной высоты N. На N +
1 строке у “ёлочки” должен быть отображен ствол из символа |
*/

import java.util.Scanner;

public class Dz3_10 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int i = input.nextInt();
        int n, z;
        for (n = 0, z = 1; n < i; n++, z += 2) {
            for (int pr = i - 1 - n; pr != 0; pr--) {
                System.out.print(" ");
            }
            for (int zv = z; zv != 0; zv--) {
                System.out.print("#");
            }
            System.out.print("\n");
        }
        for (int pr2 = i - 1; pr2 != 0; pr2--) {
            System.out.print(" ");
        }
        System.out.print("|");
    }
}