package homeWork.part3;

public class Dz3_1 {
    /*
    Напечатать таблицу умножения от 1 до 9. Входных данных нет. Многоточие в
примере ниже подразумевает вывод таблицы умножения и для остальных чисел
2, 3 и т. д.
*/
    public static void main(String[] args) {
        int i, j =0;
        for (i = 1; i < 10; i++) {
            for (j = 1; j < 10; j++){
                System.out.println(i + " x " + j + " = " + i * j);
            }
        }

    }
}
