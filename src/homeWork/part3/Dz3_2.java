package homeWork.part3;

import java.util.Scanner;

/*
На вход подается два положительных числа m и n. Найти сумму чисел между m
и n включительно.
*/
public class Dz3_2 {
    public static void main(String[] args) {
        int i, j;
        int summ = 0;
        Scanner input = new Scanner(System.in);
        i = input.nextInt();
        j = input.nextInt();
        for (int a = i; a <= j; a++)
            summ += a;

        System.out.println(summ);

    }
}
