package homeWork.part3;

import java.util.Scanner;

 /* На вход последовательно подается возрастающая последовательность из n
    целых чисел, которая может начинаться с отрицательного числа.
    Посчитать и вывести на экран, какое количество отрицательных чисел было
    введено в начале последовательности. Помимо этого нужно прекратить
    выполнение цикла при получении первого неотрицательного числа на вход.
    */

public class Dz3_9 {

    public static void main(String[] args) {
        int n = 0;

        Scanner input = new Scanner(System.in);
        for (int i = 0; i < 1000; i++) {
            int z = input.nextInt();
            if (z < 0) {
                n++;
            } else {
                System.out.println(n);
                return;
            }
        }
    }
}