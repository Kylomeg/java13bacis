package homeWork.part3;

import java.util.Scanner;

/*
На вход подается:
○ целое число n,
○ целое число p
○ целые числа a1, a2 , … an
Необходимо вычислить сумму всех чисел a1, a2, a3 … an которые строго
больше p.
*/

public class Dz3_8 {
    public static void main(String[] args) {
        int p, n;
        int summ = 0;

        Scanner input = new Scanner(System.in);
        n = input.nextInt();
        p = input.nextInt();
        int[] aArr = new int[n];
        for (int b = 0; b < n; b++) {
            aArr[b] = input.nextInt();
        }
        for (int b = 0; b < n; b++) {
            if (aArr[b] > p)
                summ += aArr[b];
        }
        System.out.println(summ);
    }
}

