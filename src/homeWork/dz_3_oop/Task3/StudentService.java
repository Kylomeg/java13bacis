package homeWork.dz_3_oop.Task3;

import homeWork.dz_3_oop.Task2.Student;
/**
 * Необходимо реализовать класс StudentService.
 * У класса должны быть реализованы следующие публичные методы:
 * ● bestStudent() — принимает массив студентов (класс Student из
 * предыдущего задания), возвращает лучшего студента (т.е. который
 * имеет самый высокий средний балл). Если таких несколько — вывести
 * любого.
 * ● sortBySurname() — принимает массив студентов (класс Student из
 * предыдущего задания) и сортирует его по фамилии.
 */

public class StudentService {

    public static Student bestStudent(Student[] students) {
        Student best = students[0];
        for (int i = 0; i < students.length; i++) {
            if (students[i].averageGrade() > best.averageGrade())
                best = students[i];
        }
        return best;
    }

    public static void sortBySurname(Student[] students) {
        int compareResult;
        Student temp;
        for (int a = 0; a < students.length; a++) {
            for (int j = 0; j < students.length - a - 1; j++) {
                compareResult = students[j].getSurname().compareTo(students[j + 1].getSurname());
                if (compareResult > 0) {
                    temp = students[j];
                    students[j] = students[j + 1];
                    students[j + 1] = temp;
                }
            }
        }
    }


//    public static void main(String[] args) {
//        Student student1 = new Student();
//        student1.setName("Максим");
//        student1.setSurname("Яковлев");
//        student1.setGrades(new int[]{0, 0, 2, 2, 3, 5, 5, 5, 3, 4});
//        Student student2 = new Student();
//        student2.setName("Дмитрий");
//        student2.setSurname("Дудка");
//        student2.setGrades(new int[]{0, 0, 4, 4, 3, 5, 5, 5, 3, 4});
//        Student student3 = new Student();
//        student3.setName("Андрей");
//        student3.setSurname("Трубник");
//        student3.setGrades(new int[]{0, 0, 3, 3, 3, 5, 5, 5, 3, 4});
//        Student student4 = new Student();
//        student4.setName("Иван");
//        student4.setSurname("Арматурин");
//        student4.setGrades(new int[]{0, 0, 4, 1, 3, 5, 5, 5, 3, 4});
//        Student[] students = new Student[4];
//        students[0] = student1;
//        students[1] = student2;
//        students[2] = student3;
//        students[3] = student4;
//        System.out.println("Best Student: " + StudentService.bestStudent(students).getName() + " " + StudentService.bestStudent(students).getSurname());
//        for (Student s : students) {
//            System.out.println(s.getSurname() + " " + s.getName());
//        }
//        StudentService.sortBySurname(students);
//        System.out.println("\nОтсортированный список: \n");
//        for (Student s : students) {
//            System.out.println(s.getSurname() + " " + s.getName());
//        }
//    }
}