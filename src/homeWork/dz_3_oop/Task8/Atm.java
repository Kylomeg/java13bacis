package homeWork.dz_3_oop.Task8;

/**
 * Реализовать класс “банкомат” Atm.
 * Класс должен:
 * ● Содержать конструктор, позволяющий задать курс валют перевода
 * долларов в рубли и курс валют перевода рублей в доллары (можно
 * выбрать и задать любые положительные значения)
 * ● Содержать два публичных метода, которые позволяют переводить
 * переданную сумму рублей в доллары и долларов в рубли
 * ● Хранить приватную переменную счетчик — количество созданных
 * инстансов класса Atm и публичный метод, возвращающий этот счетчик
 * (подсказка: реализуется через static)
 */

public class Atm {
    private static int atmAmount = 0;
    private double usdRub = 0.016;
    private double rubUsd = 60.85;

    Atm(double usdRub, double rubUsd){
        if (usdRub > 0 && rubUsd > 0) {
            this.usdRub = usdRub;
            this.rubUsd = rubUsd;
            atmAmount++;
        } else System.out.println("currency exchange must be greater than 0");
    }
    public void setUsdRub(double usdRub){
        if (usdRub > 0)
        this.usdRub = usdRub;
        else System.out.println("currency exchange must be greater than 0");
    }
    public void setRubUsd(double rubUsd){
        if (rubUsd > 0)
        this.rubUsd = rubUsd;
        else System.out.println("currency exchange must be greater than 0");
    }
    public double convertToUsd (double rub){
        double convertResult;
        convertResult = ((int)(rub * this.usdRub * 100) / 100.0);
        return convertResult;
    }
    public double convertToRub (double usd){
        double convertResult;
        convertResult = ((int)(usd * this.rubUsd * 100) / 100.0);
        return convertResult;
    }
    public int getAtmAmount(){
        return atmAmount;
    }

//    public static void main(String[] args) {
//        Atm atm1 = new Atm(0.016,60.85);
//        Atm atm2 = new Atm(0.040,24.57); // Take me back to 2007 (◕‿◕)
//        Atm atm3 = new Atm(0.016,60.85);
//        atm3.setRubUsd(65.85);
//        System.out.println("количество банкоматов: " + atm1.getAtmAmount());
//        System.out.println(atm2.convertToRub(100));
//        System.out.println(atm1.convertToRub(100));
//        System.out.println(atm3.convertToRub(100));
//        System.out.println(atm2.convertToUsd(100));
//        System.out.println(atm1.convertToUsd(100));
//        System.out.println(atm3.convertToUsd(105));
//    }
}
