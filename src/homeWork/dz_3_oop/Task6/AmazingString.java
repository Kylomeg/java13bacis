package homeWork.dz_3_oop.Task6;

/**
 * Необходимо реализовать класс AmazingString, который хранит внутри себя
 * строку как массив char и предоставляет следующий функционал:
 * Конструкторы:
 * ● Создание AmazingString, принимая на вход массив char
 * ● Создание AmazingString, принимая на вход String
 * Публичные методы (названия методов, входные и выходные параметры
 * продумать самостоятельно). Все методы ниже нужно реализовать “руками”, т.е.
 * не прибегая к переводу массива char в String и без использования стандартных
 * методов класса String.
 * ● Вернуть i-ый символ строки
 * ● Вернуть длину строки
 * ● Вывести строку на экран
 * ● Проверить, есть ли переданная подстрока в AmazingString (на вход
 * подается массив char). Вернуть true, если найдена и false иначе
 * ● Проверить, есть ли переданная подстрока в AmazingString (на вход
 * подается String). Вернуть true, если найдена и false иначе
 * ● Удалить из строки AmazingString ведущие пробельные символы, если
 * они есть
 * ● Развернуть строку (первый символ должен стать последним, а
 * последний первым и т.д.)
 */
public class AmazingString {
    private char[] str;

    AmazingString(char[] str) {
        this.str = str;
    }

    AmazingString(String s) {
        this.str = s.toCharArray();
    }

    public char getStrAt(int n) {
        return this.str[n];
    }

    public int getStrLength() {
        return this.str.length;
    }

    public void printStr() {
        for (int a = 0; a < str.length; a++)
            System.out.print(str[a]);
        System.out.print("\n");
    }

    public boolean checkStringStr(String s) {
        boolean stringAtStr = false;
        for (int a = 0; a < str.length; a++) {
            //Проверяем есть ли первый символ переданной строки в массиве и при этом длинна строки поместится в оставшемся массиве.
            if (s.charAt(0) == this.str[a] && s.length() < str.length - a + 1) {
                for (int b = 0; s.charAt(b) == str[a + b] && b < s.length(); ++b) {
                    //Пока символы последовательно равны выполняется цикл
                    if (b + 1 == s.length()) {// Если последний символ переданной строки равен - строка присутствует...
                        stringAtStr = true;
                        break;
                    }
                }
            }

        }
        System.out.println(stringAtStr);//для самопроверки, можно закомментить
        return stringAtStr;
    }

    public boolean checkCharStr(char[] c) {
        boolean charAtStr = false;
        for (int a = 0; a < str.length; a++) {
            if (c[0] == this.str[a] && c.length < str.length - a + 1) {
                for (int b = 0; c[b] == str[a + b]; b++) {
                    if (b + 1 == c.length) {
                        charAtStr = true;
                        break;
                    }
                }
            }

        }
        System.out.println(charAtStr); //для самопроверки, можно закомментить
        return charAtStr;
    }
    public void spaceRemoveStr(){
        while (str[0] == ' '){
            char[] temp = new char[str.length - 1];
            for (int a = 0; a < temp.length; a++)
                temp[a] = str[a + 1];
            str = temp;
        }
    }
    public void reverseStr(){
        char[] temp = new char[str.length];
        for (int a = 0; a < str.length; a++)
            temp[str.length - 1 - a] = str[a];
        str = temp;
    }

//    public static void main(String[] args) {
//        AmazingString amz = new AmazingString("  I haven't slept for two days");
//        System.out.println(amz.getStrAt(6));
//        System.out.println(amz.getStrLength());
//        amz.printStr();
//        amz.spaceRemoveStr();
//        System.out.println();
//        amz.printStr();
//        amz.checkStringStr(" days");
//        amz.checkCharStr(new char[]{'s', 'l', 'e', 'p', 't','f'});
//        amz.reverseStr();
//        amz.printStr();
//    }
}
