package homeWork.dz_3_oop.Task5;

public class TestDayOfTheWeek {
    public static void main(String[] args) {
        DayOfTheWeek[] week = new DayOfTheWeek[7];
        week[0] = new DayOfTheWeek((byte) 1, "Monday");
        week[1] = new DayOfTheWeek((byte) 2, "Tuesday");
        week[2] = new DayOfTheWeek((byte) 3, "Wednesday");
        week[3] = new DayOfTheWeek((byte) 4, "Thursday");
        week[4] = new DayOfTheWeek((byte) 5, "Friday");
        week[5] = new DayOfTheWeek((byte) 6, "Saturday");
        week[6] = new DayOfTheWeek((byte) 7, "Sunday");

        for (DayOfTheWeek s : week) {
            System.out.println(s.getNum() + " " + s.getWeekDay());
        }

    }
}