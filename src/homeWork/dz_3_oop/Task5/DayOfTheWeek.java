package homeWork.dz_3_oop.Task5;

/**
 * Необходимо реализовать класс DayOfWeek для хранения порядкового номера
 * дня недели (byte) и названия дня недели (String).
 * Затем в отдельном классе в методе main создать массив объектов DayOfWeek
 * длины 7. Заполнить его соответствующими значениями (от 1 Monday до 7
 * Sunday) и вывести значения массива объектов DayOfWeek на экран.
 * Пример вывода:
 * 1 Monday
 * 2 Tuesday
 * …7
 * Sunday
 */

public class DayOfTheWeek {
    private byte num;
    private String weekDay;

    DayOfTheWeek(byte n, String d){
        this.num = n;
        this.weekDay = d;
    }
    public byte getNum(){
        return num;
    }
    public String getWeekDay(){
        return weekDay;
    }

}
