package homeWork.dz_3_oop.Task7;

/**
 * Реализовать класс TriangleChecker, статический метод которого принимает три
 * длины сторон треугольника и возвращает true, если возможно составить из них
 * треугольник, иначе false. Входные длины сторон треугольника — числа типа
 * double. Придумать и написать в методе main несколько тестов для проверки
 * работоспособности класса (минимум один тест на результат true и один на
 * результат false)
 */

public class TriangleChecker {
    private double a = 0;
    private double b = 0;
    private double c = 0;

//    TriangleChecker(double a, double b, double c) {
//        this.a = a;
//        this.b = b;
//        this.c = c;
//    }

    public static boolean check(double a, double b, double c) {
        boolean triangle = false;
        if (a + b > c)
            if (a + c > b)
                if (b + c > a)
                    triangle = true;
        return triangle;
    }

    public static void main(String[] args) {
        System.out.println(TriangleChecker.check(10.0, 9.0, 1.1));
        System.out.println(TriangleChecker.check(1.1, 9.0, 10.0));
        System.out.println(TriangleChecker.check(10.0, 9.0, 1.0));
        System.out.println(TriangleChecker.check(10.0, 8.8, 1.1));
        System.out.println(TriangleChecker.check(10.0, 9.0, 1.1));
        System.out.println(TriangleChecker.check(8.9, 10.0, 1.1));
        System.out.println(TriangleChecker.check(1, 100, 1)); // Fixed case where the sides are equal

    }
}
