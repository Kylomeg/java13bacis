package homeWork.dz_3_oop.Task2;

/**
 * Необходимо реализовать класс Student.
 * У класса должны быть следующие приватные поля:
 * ● String name — имя студента
 * ● String surname — фамилия студента
 * ● int[] grades — последние 10 оценок студента. Их может быть меньше, но
 * не может быть больше 10.
 * И следующие публичные методы:
 * ● геттер/сеттер для name
 * ● геттер/сеттер для surname
 * ● геттер/сеттер для grades
 * ● метод, добавляющий новую оценку в grades. Самая первая оценка
 * должна быть удалена, новая должна сохраниться в конце массива (т.е.
 * массив должен сдвинуться на 1 влево).
 * ● метод, возвращающий средний балл студента (рассчитывается как
 * среднее арифметическое от всех оценок в массиве grades)
 */

public class Student {
    private String name;
    private String surname;
    private int[] grades = new int[10];

    public Student() {
    }

    public void setName(String s) {
        name = s;
    }

    public void setSurname(String s) {
        surname = s;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int[] getGrades() {
        return grades;
    }

    public void setGrades(int[] grades) {
        this.grades = grades;
    }

    public void addGrade(int a) {
        for (int i = 1; i < 10; i++)
            grades[i - 1] = grades[i];
        grades[9] = a;
    }

    public double averageGrade() {
        int sum = 0;
        int numOfGrades = 0;
        for (int i = 0; i < 10; i++) {
            if (grades[i] != 0)
                numOfGrades++;
        }
        for (int i = 0; i < 10; i++) {
            sum += grades[i];
        }
        return (sum * 10 / numOfGrades / 10.0);
    }

//    public static void main(String[] args) { //Проверка класса Student
//        Student student1 = new Student();
//        student1.setName("Максим");
//        student1.setSurname("Иванов");
//        student1.setGrades(new int[]{0, 0, 2, 2, 3, 5, 5, 5, 3, 4});
//        System.out.println(student1.getName());
//        System.out.println(student1.getSurname());
//        String gradesString = Arrays.toString(student1.getGrades());
//        System.out.println(gradesString);
//        student1.addGrade(5);
//        gradesString = Arrays.toString(student1.getGrades());
//        System.out.println(gradesString);
//        System.out.println(student1.averageGrade());
//
//    }
}
