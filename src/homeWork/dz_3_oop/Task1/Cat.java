package homeWork.dz_3_oop.Task1;

/**
 * Необходимо реализовать класс Cat.
 * У класса должны быть реализованы следующие приватные методы:
 * ● sleep() — выводит на экран “Sleep”
 * ● meow() — выводит на экран “Meow”
 * ● eat() — выводит на экран “Eat”
 * И публичный метод:
 * status() — вызывает один из приватных методов случайным образом.
 */

public class Cat {
    Cat() {
    }

    private void sleep() {
        System.out.println("Sleep");
    }

    private void meow() {
        System.out.println("Meow");     //Simon says Meow
    }

    private void eat() {
        System.out.println("Eat");
    }

    public void getStatus() {
        int random = (int) (3 * Math.random());
        if (random < 1)
            this.sleep();
        else if (random < 2)
            this.meow();
        else
            this.eat();


    }

//    public static void main(String[] args) {
//        Cat cat1 = new Cat();
//        cat1.getStatus();
//    }
}

