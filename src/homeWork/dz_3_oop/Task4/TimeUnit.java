package homeWork.dz_3_oop.Task4;

import java.text.DecimalFormat;

/**
 * Необходимо реализовать класс TimeUnit с функционалом, описанным ниже
 * (необходимые поля продумать самостоятельно). Обязательно должны быть
 * реализованы валидации на входные параметры.
 * Конструкторы:
 * ● Возможность создать TimeUnit, задав часы, минуты и секунды.
 * ● Возможность создать TimeUnit, задав часы и минуты. Секунды тогда
 * должны проставиться нулевыми.
 * ● Возможность создать TimeUnit, задав часы. Минуты и секунды тогда
 * должны проставиться нулевыми.
 * Публичные методы:
 * ● Вывести на экран установленное в классе время в формате hh:mm:ss
 * ● Вывести на экран установленное в классе время в 12-часовом формате
 * (используя hh:mm:ss am/pm)
 * ● Метод, который прибавляет переданное время к установленному в
 * TimeUnit (на вход передаются только часы, минуты и секунды).
 */

public class TimeUnit {
    private static int h;
    private static int m;
    private static int s;

    TimeUnit(int h, int m, int s) {
        if (h >= 0 && h < 25 && m >= 0 && m < 61 && s >= 0 && s < 61) {
            this.h = h;
            this.m = m;
            this.s = s;
        } else System.out.println("Incorrect value");
    }

    TimeUnit(int h, int m) {
        if (h >= 0 && h < 25 && m >= 0 && m < 61) {
            this.h = h;
            this.m = m;
            this.s = 0;
        } else System.out.println("Incorrect value");
    }

    TimeUnit(int h) {
        if (h >= 0 && h < 25) {
            this.h = h;
            this.m = 0;
            this.s = 0;
        } else System.out.println("Incorrect value");
    }

    public void getTime() {
        DecimalFormat df = new DecimalFormat("00");
        System.out.println(df.format(h) + ":" + df.format(m) + ":" + df.format(s));
    }

    public void getTimeMeridiem() {
        DecimalFormat df = new DecimalFormat("00");
        int meridiemH = this.h;
        String ampm = "AM";
        if (meridiemH > 12) {
            ampm = "PM";
            meridiemH -= 12;
        }
        System.out.println(df.format(meridiemH) + ":" + df.format(m) + ":" + df.format(s) + " " + ampm);
    }
    public void addTime(int h1, int m1, int s1){
        if (h >= 0 && h < 25 && m >= 0 && m < 61 && s >= 0 && s < 61) {
            s += s1;
            if (s > 60){
                m++;
                s %= 60;
            }
            m += m1;
            if (m > 60){
                h++;
                m %= 60;
            }
            h += h1;
            if (h > 24)
                h %= 24;

        }  else System.out.println("Incorrect value");
    }

//    public static void main(String[] args) {
//        TimeUnit time1 = new TimeUnit(22, 49, 15);
//        time1.getTime();
//        time1.getTimeMeridiem();
//        time1.addTime(5, 15, 40);
//        time1.getTime();
//    }
}
