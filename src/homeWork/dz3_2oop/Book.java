package homeWork.dz3_2oop;

public class Book {
    private String bookName;
    private String author;
    private Reader reader = null;
    private boolean inUse;

    public Book (String bookName, String author){
        this.bookName = bookName;
        this.author = author;
    }

    public String getBookName() {
        return bookName;
    }

    public String getAuthor() {
        return author;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Reader getReader() {
        return reader;
    }

    public void setReader(Reader reader) {
        this.reader = reader;
    }

    public boolean isInUse() {
        return inUse;
    }

    public void setInUse(boolean inUse) {
        this.inUse = inUse;
    }

//    public boolean inUse() {
//        return inUse;
//    }

}

