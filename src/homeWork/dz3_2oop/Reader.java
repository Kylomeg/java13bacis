package homeWork.dz3_2oop;

import java.util.ArrayList;
import java.util.List;

public class Reader {
    private String readerName;
    private Book book = null;
    private static int readerId = 0;

    public Reader() {

    }
    public Reader(String readerName) {
        this.readerName = readerName;

    }
public int genReaderId() {
        return ++readerId;
}
    public String getReaderName() {
        return readerName;
    }
    public void setReaderName(String readerName){
        this.readerName = readerName;
    }

    public Book getBook() {
        return book;
    }
    public void setBook (Book book){
        this.book = book;
        this.book.setInUse(true);
//        this.book.setReader();
    }
    List<Reader> readersArray = new ArrayList<>(1);

}

