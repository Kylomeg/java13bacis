package homeWork.dz3_2oop;

import java.util.List;

public class TestingLibrary {
    public static void main(String[] args) {
        Library library = new Library();

        library.fillBooks(new Book("Война и мир", "Толстой")); //Первичное наполнение библиотеки.

        Book book1 = new Book("Властелин колец", "Толкин");
        Book book2 = new Book("Гарри поттер", "Дж. Роалинг");
        Book book3 = new Book("Азбука", "Коллектив авторов");
        Book book4 = new Book("Азбука. Полное руководство", "Коллектив авторов");
        Book book5 = new Book("Азбука для профессионалов", "Коллектив авторов");
        Book book6 = new Book("Геометрия 9", "Семён Косинус");

        library.addBook(book1);
        library.addBook(book2);
        library.addBook(book3);
        library.addBook(book4);
        library.addBook(book5);
        library.addBook(book6);

        library.addBook(book2);

        Reader reader1 = new Reader("Афанасий Петрович");
        Reader reader2 = new Reader("Инокентий Смоктуновский");
        Reader reader3 = new Reader("Дима");

        library.giveBookTo(book3, reader1);
        library.giveBookTo(book6, reader2);
        library.giveBookTo(book5, reader3);

        System.out.println();
        library.allInLib();

        System.out.println();
        library.deleteBook(book1);
        library.deleteBook(book3);

        System.out.println();
        library.allInLib();
        System.out.println();

        library.getBooksByAuthor("Коллектив авторов");

    }


}
