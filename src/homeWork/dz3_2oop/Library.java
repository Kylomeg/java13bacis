package homeWork.dz3_2oop;

import java.util.ArrayList;
import java.util.List;

public class Library {
private static ArrayList<Book> bookList = new ArrayList<>(1) ;
private List<Reader> readerList = new ArrayList<>(1);

    public void fillBooks(Book book) {
        bookList.add(book);
    }
    public void addBook(Book book) {
        if(!bookList.contains(book)) {
            bookList.add(book);
            System.out.println("Книга \"" + book.getBookName() + "\" добавлена успешно.");
        } else {
            System.out.println("Такая книга уже существует в библиотеке.");
        } // Добавление книги (1)
    }

    public static Book getBook(String name) {
        Book book = null;
        for (Book book1: bookList) {
            if(book1.getBookName().equals(name)) {
                book = book1;
            }
        }
        return book; // Возврат книги по названию (3)
    }

public void deleteBook(Book bookName) {
    boolean a = false, b = false;
    for (Book el : bookList) {
        if (bookName.getBookName().equals(el.getBookName()) &&
                bookName.getAuthor().equals(el.getAuthor())) {
            a = true;
            if (bookName.isInUse() == false) {
                b = true;
            }
        }
    }
    if (a && b) {
        bookList.remove(bookName);
        System.out.printf("Книга %s удалена" + "\n", bookName.getBookName());
    } else if (!a) {
        System.out.println("Книги нет в библиотеке");
    } else {
        System.out.println("Книга находится у читателя");
    }
}


    public List<Book> getBooksByAuthor(String author1) {
        List<Book> sortByAuthor = new ArrayList<>();
        for (Book b: bookList) {
            if(b.getAuthor().equals(author1)) {
                sortByAuthor.add(b);
                System.out.println(b.getBookName()); // Для теста
            }
        }
        return sortByAuthor;// Возврат списка объектов книг по автору (4)
    }
    public void allInLib(){
        for (Book b: bookList) {
            System.out.println("\"" + b.getBookName() + "\"" + "\t у читателя: " + b.isInUse());
        } //Вывод всех книг в библиотеке
    }
    public void giveBookTo(Book nameBook, Reader reader) {
        if(!nameBook.isInUse()) {
            reader.setBook(nameBook);
            nameBook.setReader(reader);
            System.out.println("Книга \"" + nameBook + "\" выдана посетителю: " + reader.getReaderName());
        }
    }

}
