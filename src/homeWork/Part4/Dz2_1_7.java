package homeWork.Part4;

import java.util.Arrays;
import java.util.Scanner;

/*
На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов, отсортированный по возрастанию.
Необходимо создать массив, полученный из исходного возведением в квадрат
каждого элемента, упорядочить элементы по возрастанию и вывести их на
экран.
*/
public class Dz2_1_7 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        int[] m1 = new int[n];
        for (int a = 0; a < n; a ++)
            m1[a] = input.nextInt();
        for (int a = 0; a < n; a ++)
            m1[a] = (int) Math.pow(m1[a], 2);
        Arrays.sort(m1);
        for (int a = 0; a < n; a ++)
            System.out.print(m1[a] + " ");
    }
}
