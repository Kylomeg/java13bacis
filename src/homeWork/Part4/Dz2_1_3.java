package homeWork.Part4;

import java.util.Scanner;

/*      На вход подается число N — длина массива. Затем передается массив
        целых чисел (ai) из N элементов, отсортированный по возрастанию. После этого
        вводится число X — элемент, который нужно добавить в массив, чтобы
        сортировка в массиве сохранилась.
        Необходимо вывести на экран индекс элемента массива, куда нужно добавить
        X. Если в массиве уже есть число равное X, то X нужно поставить после уже
        существующего.
        */
public class Dz2_1_3 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pos, lastPos = 0;
        int n = input.nextInt();
        int[] i = new int[n];
        for (int p = 0; p < i.length; p++)
            i[p] = input.nextInt();
        int x = input.nextInt();
        for (pos = 0; pos < i.length; pos++)
            if (i[pos] > x) {
                lastPos = pos;
                break;
            }
        System.out.println(lastPos);



    }
}
