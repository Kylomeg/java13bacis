package homeWork.Part4;

import java.util.Scanner;

/*
На вход подается число N — длина массива. Затем передается массив
вещественных чисел (ai) из N элементов.
Необходимо реализовать метод, который принимает на вход полученный
массив и возвращает среднее арифметическое всех чисел массива.
Вывести среднее арифметическое на экран.
 */
public class Dz2_1_1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        double[] m = new double[n];
        for (int p = 0; p < m.length; p++)
            m[p] = input.nextDouble();
        System.out.println(average(m));

    }

    public static double average(double in[]) { //Метод вычисляет среднее арифметическое массива
        double aver = 0;
        for (int i = 0; i < in.length; i++)
            aver += in[i];
        aver /= in.length;
        return aver;
    }
}
