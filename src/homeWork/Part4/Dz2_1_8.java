package homeWork.Part4;
/*
На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов. После этого передается число M.
Необходимо найти в массиве число, максимально близкое к M (т.е. такое число,
для которого |ai - M| минимальное). Если их несколько, то вывести
максимальное число.
*/
import java.util.Scanner;

public class Dz2_1_8 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        int[] m1 = new int[n];
        int minN = 1001;
        int min = 0;
        for (int a = 0; a < n; a ++)
            m1[a] = input.nextInt();
        int m = input.nextInt();
        for (int a = 0; a < n; a ++) {
            if (Math.abs(m1[a]) - Math.abs(m) < Math.abs(min)) {
                minN = a;
                min = m1[a] - m;
            }
        }
        System.out.println(m1[minN]);
    }
}
