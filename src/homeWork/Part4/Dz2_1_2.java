package homeWork.Part4;

import java.util.Arrays;
import java.util.Scanner;

/* На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов. После этого аналогично передается второй
массив (aj) длины M.
Необходимо вывести на экран true, если два массива одинаковы (то есть
содержат одинаковое количество элементов и для каждого i == j элемент ai ==
aj). Иначе вывести false.

 */
public class Dz2_1_2 {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        int[] i = new int[n];
        for (int p = 0; p < i.length; p++)
            i[p] = input.nextInt();
        n = input.nextInt();
        int[] j = new int[n];
        for (int p = 0; p < j.length; p++)
            j[p] = input.nextInt();
        System.out.println(Arrays.equals(i, j));
    }
}
