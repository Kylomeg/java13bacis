package homeWork.Part4;

import java.util.Scanner;

/*
На вход подается число N — длина массива. Затем передается массив
строк из N элементов (разделение через перевод строки). Каждая строка
содержит только строчные символы латинского алфавита.
Необходимо найти и вывести дубликат на экран. Гарантируется что он есть и
только один.
*/
public class Dz2_1_9 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        String[] m1 = new String[n];
        for (int a = 0; a < n; a ++)
            m1[a] = input.next();
        for (int a = 0; a < n; a++) {
            for (int b = 0; b < n; b++) {
                if (a == b)
                    b++;
                else
                    if (m1[a].equals(m1[b])) {
                        System.out.println(m1[a]);
                        a = n - 1; // Останавливаем цикл после первого совпадения
                    }
                }
            }
        }
    }

