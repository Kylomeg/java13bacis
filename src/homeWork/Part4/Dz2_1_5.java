package homeWork.Part4;

import java.util.Scanner;

/*
На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов. После этого передается число M — величина
сдвига.
Необходимо циклически сдвинуть элементы массива на M элементов вправо.
*/
public class Dz2_1_5 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        int[] mass = new int[n];
        for (int p = 0; p < mass.length; p++)
            mass[p] = input.nextInt();
        int sm = input.nextInt();
if (sm != 0) {
    do {
        int temp = mass[mass.length - 1]; // Сохраняет последний элемент
        // Сдвинуть элементы вправо
        for (int i = mass.length - 1; i != 0; i--) {
            mass[i] = mass[i - 1];
        }
        // Переместить значение последнего элемента, чтобы заполнить первую позицию
        mass[0] = temp;
        sm--;
    } while (sm != 0);
}
        for (int z = 0; z < mass.length; z++)
            System.out.print(mass[z] + " ");
    }
}




