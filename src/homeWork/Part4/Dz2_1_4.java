package homeWork.Part4;

/*
На вход подается число N — длина массива. Затем передается массив
целых чисел (ai) из N элементов, отсортированный по возрастанию.
Необходимо вывести на экран построчно сколько встретилось различных
элементов. Каждая строка должна содержать количество элементов и сам
элемент через пробел.
*/

import java.util.Scanner;

public class Dz2_1_4 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        int rep = 1, p = 0;
        int[] i = new int[n];
        for (int z = 0; z < i.length; z++)
            i[z] = input.nextInt();
        int a = i[0], b = 0;
        for (p = 1; p < i.length; p++) {
            b = i[p];
            if (a == b) {
                rep++;
//                a = i[p];
            } else {
                System.out.println(rep + " " + a);
            a = i[p];
                rep = 1;
            }

        }
        System.out.println(rep + " " + a);

    }

}

