package homeWork.part1;

import java.util.Scanner;

public class Dz1_2 {
    public static void main(String[] args) {
        int a, b;
        double rms;
        Scanner input = new Scanner(System.in);
        a = input.nextInt();
        b = input.nextInt();
        rms = Math.sqrt((Math.pow(a, 2) + Math.pow(b, 2)) / 2.0);
        System.out.println(rms);
    }
}
