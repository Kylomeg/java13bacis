package homeWork.part1;

import java.util.Scanner;

public class Dz1_1 {
    public static void main(String[] args) {
        int r;
        double volume;

        Scanner input = new Scanner(System.in);
        r = input.nextInt();
        volume = (4.0 / 3) * Math.PI * Math.pow(r, 3);
        System.out.println(volume);


    }
}
