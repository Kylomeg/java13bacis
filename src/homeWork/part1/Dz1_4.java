package homeWork.part1;

import java.util.Scanner;

public class Dz1_4 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int count, hour, min;
        count = input.nextInt();
        min = count / 60 % 60;
        hour = count / 60 / 60;
        System.out.println(hour + " " + min);

    }
}
